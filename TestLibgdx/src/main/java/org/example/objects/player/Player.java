package org.example.objects.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import static org.example.helper.Constants.PPM;

public class Player extends GameEntity {

    // count amount of jumps (only want double, not triple/friple... etc.)
    private int jumpCounter;

    public Player(float width, float height, Body body) {
        super(width, height, body);
        this.speed = 10f;
        this.jumpCounter = 0;

    }

    @Override
    public void update() {
        x = body.getPosition().x * PPM;
        y = body.getPosition().y * PPM;

        checkUserInput();
    }

    @Override
    public void render(SpriteBatch batch) {

    }

    public void checkUserInput() {
        // reset the current velocity x to 0 to stop the movement
        velX = 0;
        if(Gdx.input.isKeyPressed(Input.Keys.D))
            velX = 1;
        if(Gdx.input.isKeyPressed(Input.Keys.A))
            velX = -1;

        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && jumpCounter < 2) {
            float force = body.getMass() * 15;
            body.setLinearVelocity(body.getLinearVelocity().x, 0);
            body.applyLinearImpulse(new Vector2(0, force), body.getPosition(), true);
            jumpCounter++;
        }

        // reset jumpcounter [maybe fix this so there is some collision detection]
        // we have hit the floor after jump
        if(body.getLinearVelocity().y == 0)
            jumpCounter = 0;

        body.setLinearVelocity(velX * speed, body.getLinearVelocity().y);
    }
}
