# testLibgdx

## Testing of Tiled program

The primary classes for the implementation of a tiledmap is 
- in the helper folder:
BodyHelperService.java and
TileMapHelper.java
- in the objects/player folder:
GameEntity.java and
Player.java

a Player, OrthogonalTiledMapRenderer and TileMapHelper object also gets initialized in the GameScreen.java file and updated, etc.
Like (for example) in the constructor:
        this.tileMapHelper = new TileMapHelper(this);
        this.orthogonalTiledMapRenderer = tileMapHelper.setupMap();
